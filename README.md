## release-it-poc

This is a proof of concept of using [Release It!](https://github.com/release-it/release-it) with [NX](https://nx.dev) for monorepo releases. 

Step 1:
- install npm with homebrew: `brew install npm`
- add a Nx workspace to the repository `npx nx@latest init` 
